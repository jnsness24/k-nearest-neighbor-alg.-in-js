/**
 * Created by Jonas on 14.06.2017.
 */

"use strict";

var amountKNearest = 3;
var splicedData = [];

var data = [
    {
        movieTitle: "California Man",
        nrOfKicks: 3,
        nrOfKisses: 104,
        typeOfMovie: "Romance"
    },
    {
        movieTitle: "Hes not reall into dudes",
        nrOfKicks: 2,
        nrOfKisses: 101,
        typeOfMovie: "Romance"
    },
    {
        movieTitle: "Beautiful Woman",
        nrOfKicks: 1,
        nrOfKisses: 81,
        typeOfMovie: "Romance"
    },
    {
        movieTitle: "Kevin Longblade",
        nrOfKicks: 101,
        nrOfKisses: 10,
        typeOfMovie: "Action"
    },
    {
        movieTitle: "Robo Slayer 3000",
        nrOfKicks: 99,
        nrOfKisses: 5,
        typeOfMovie: "Action"
    },
    {
        movieTitle: "Amped",
        nrOfKicks: 98,
        nrOfKisses: 2,
        typeOfMovie: "Action"
    }
];

var unknownMovie = {
    movieTitle: "?",
    nrOfKicks: 100,
    nrOfKisses: 50,
    typeOfMovie: null
};


var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");

initCalculation();
function initCalculation() {
    /**
     * triggers the distance calculation and adds this value "distanceFromUnknown" to the object itself
     */
    setDistancesFromUnknownMovie();
    data.sort(compare);

    /**
     * set the type of the new Movie with Majority Function
     * @type {*}
     */
    unknownMovie.typeOfMovie = getMajorityGenre(data, amountKNearest);
    console.log("The unknown Movie therefore has the genre of " + unknownMovie.typeOfMovie);

    draw();
};


function setDistancesFromUnknownMovie() {
    for (let obj of data) {
        var distance = calculateDistances(unknownMovie.nrOfKicks, obj.nrOfKicks, unknownMovie.nrOfKisses, obj.nrOfKisses);
        obj.distanceFromUnknown = distance;
    }
}

function compare(a, b) {
    const distanceA = a.distanceFromUnknown;
    const distanceB = b.distanceFromUnknown;

    var comparison = 0;
    if (distanceA > distanceB) {
        comparison = 1;
    } else if (distanceA < distanceB) {
        comparison = -1;
    }

    return comparison;
}

/**
 * takes the data Object Array, splices it in the k Nearest, and calculates which Genre appears more often - then returns the majority
 * @param data
 * @param k
 * @returns {*}
 */
function getMajorityGenre(data, k) {

    splicedData =[];
    for (var i = 0; i < k; i++) {
        splicedData[i] = data[i];
    }

    var amountRomance = 0, amountAction = 0, majorityGenre;
    for (let obj of splicedData) {
        if (obj.typeOfMovie === "Romance") {
            amountRomance++;
        }
        else {
            amountAction++;
        }
    }

    if (amountRomance > amountAction) {
        majorityGenre = "Romance";
    }
    else {
        majorityGenre = "Action";
    }

    for (var i = 0; i < splicedData.length; i++) {
        console.log("the " + i + " nearest Neighbor is: " + splicedData[i].movieTitle);
    }

    console.log("The k: " + k + " nearest Neighbors have the majority of " + majorityGenre);

    return majorityGenre;
}

/**
 * sets a red color for romance movies and a blue color for action movies
 * then paints the dots in the canvas
 * also strokes Lines to the nearest Neighbors
 */
function draw() {

    // Clear the Canvas before draw the new Data
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    for (let obj of data) {
        ctx.fillStyle = checkColor(obj.typeOfMovie);
        ctx.fillRect(obj.nrOfKicks, obj.nrOfKisses, 5, 5);
    }

    /**
     * unknown movie
     * @type {string}
     */
    ctx.fillStyle = checkColor(unknownMovie.typeOfMovie);
    ctx.fillRect(unknownMovie.nrOfKicks, unknownMovie.nrOfKisses, 5, 5);

    for (var i = 0; i < amountKNearest; i++) {
        strokeLines(unknownMovie.nrOfKicks, unknownMovie.nrOfKisses, splicedData[i].nrOfKicks,splicedData[i].nrOfKisses,checkColor(unknownMovie.typeOfMovie));
    }

    console.log("=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x=x");


    /**
     * stroke lines from two Points - the nearest from our unknown Movie
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     */
    function strokeLines(x1,y1,x2,y2,color) {
        ctx.beginPath();
        ctx.moveTo(x1,y1);
        ctx.lineTo(x2,y2);
        ctx.strokeStyle = color;
        ctx.stroke();
    }

    /**
     * fills the color with blue or red, depending on the typeOfMovie
     * @param typeOfMovie
     * @returns {*}
     */
    function checkColor(typeOfMovie) {
        var dotColor;
        if (typeOfMovie == "Romance") {
            dotColor = "#FF0000";
        }
        else {
            dotColor = "#0000FF"
        }
        return dotColor;
    }
}

/**
 * returns the vectors distance from two points
 * @param coordX1
 * @param coordX2
 * @param coordY1
 * @param coordY2
 * @returns {number}
 */
function calculateDistances(coordX1, coordX2, coordY1, coordY2) {
    var distance = Math.sqrt(Math.pow(coordX1 - coordX2, 2) + Math.pow(coordY1 - coordY2, 2));
    return distance;
}

/**
 * Setter for X
 * @param x
 */
function setXOfUnknownMovie(x) {
    unknownMovie.nrOfKicks = x;
    // ctx.clearRect(0, 0, canvas.width, canvas.height);
    initCalculation();
}

/**
 * Setter for Y
 * @param y
 */
function setYOfUnknownMovie(y) {
    unknownMovie.nrOfKisses = y;
    // ctx.clearRect(0, 0, canvas.width, canvas.height);
    initCalculation();
}


